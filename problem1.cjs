function getCarById(inventory, id) {
    let car = []
    if(!Array.isArray(inventory) || inventory.length == 0)
    {
        return car;
    }
    if(id === undefined)
    {
        return car;
    }
    for (let index = 0; index < inventory.length; index++) {
        let car = inventory[index];
        if (car.id == id) {
            return car;
        }
    }
    return car;
}


module.exports = getCarById;