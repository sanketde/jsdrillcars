function getBMWAudiCars(inventory) {
    
    let car = []
    if(!Array.isArray(inventory) || inventory.length == 0)
    {
        return car;
    }
    let bmwAndAudiCars = []
    for (let index = 0; index < inventory.length; index++) {
        let car = inventory[index];
        if (car.car_make === "BMW" || car.car_make === "Audi") {
            bmwAndAudiCars.push(car);
        }

    }
    return bmwAndAudiCars;
}


module.exports = getBMWAudiCars;