function getLastCar(inventory) {
    let car = []
    if(!Array.isArray(inventory) || inventory.length == 0)
    {
        return car;
    }
    return inventory[inventory.length - 1];
}


module.exports = getLastCar;