function getSortedCarListByModel(inventory) {
    let car = []
    if(!Array.isArray(inventory) || inventory.length == 0)
    {
        return car;
    }
    let carModelList = [];
    for (let index = 0; index < inventory.length; index++) {
        let car = inventory[index];
        carModelList.push(car.car_model);

    }
    carModelList.sort()
    return carModelList;
}


module.exports = getSortedCarListByModel;