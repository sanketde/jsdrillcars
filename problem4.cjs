function getAllCarYears(inventory) {
    let car = []
    if(!Array.isArray(inventory) || inventory.length == 0)
    {
        return car;
    }
    let carYears = [];
    for (let index = 0; index < inventory.length; index++) {
        let car = inventory[index];
        carYears.push(car.car_year);

    }
    return carYears;
}


module.exports = getAllCarYears;