let getAllCarYears = require('./problem4.cjs');

function getOldCarCount(inventory) {
    let car = []
    if(!Array.isArray(inventory) || inventory.length == 0)
    {
        return car;
    }
    let count = 0;
    let carYears = getAllCarYears(inventory);
    for (let index = 0; index < carYears.length; index++) {
        let year = carYears[index];
        if (year < 2000) {
            count++;
        }

    }
    return count;
}


module.exports = getOldCarCount;